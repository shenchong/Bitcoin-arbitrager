# 如何部署？
1. Python版本 python3.4 [下载链接](https://www.python.org/downloads/release/python-344/)
2. 数据库版本 Mysql5.7 [下载链接](http://dev.mysql.com/downloads/windows/installer/)
3. Workbench 6.3.7 [下载地址](http://dev.mysql.com/downloads/workbench/)
3. Python连接Mysql驱动 mysql-connector-python 2.0.4 [下载链接](https://pypi.python.org/pypi/mysql-connector-python/2.0.4)
    或者直接使用pip命令安装，命令：easy_install mysql-connector-python
    如果命令执行失败，则下载包，解压后命令行下打开此目录，然后运行python setup.py install
4. 数据库脚本在Database.md中，用来恢复数据库
5. config.py为套利程序的参数设置文件
6. 运行arbitrager_haobtc.py即可开启套利，
    命令：python "arbitrager_haobtc.py路径"(python 将arbitrager_haobtc.py文件拖入cmd窗口即可）
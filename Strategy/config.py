# 命名规则，.py文件名都为小写，类名首字母为大写，其余的都为小写
exchanges = {
    "Okcoin",
    "Haobtc",
    #"Huobi",
    #"Chbtc",
    #"Btcc",
    #"Btctrade",
    "Bitbays"
}

HUOBI_API_KEY = 'fbd6d15d-24955e6a-34dfbb08-402af'
HUOBI_SECRET_TOKEN = '8c92b3e2-2a575058-93e6dbe9-93d48'

OKCOIN_API_KEY = '8722573e-32e4-4167-9c00-5366088c5adc'
OKCOIN_SECRET_TOKEN = 'F10760B51DE71C01BE4F414AF4532FB3'

CHBTC_API_KEY = 'd7caf970-503c-4366-a7e9-af04e02baf10'
CHBTC_SECRET_TOKEN = '0cb2a7a8-e32e-441c-8c21-96fa47bbd76a'

BTCTRADE_API_KEY = '53qpr-xmmju-cu664-qq8wx-vvkai-8dics-wgvm5'
BTCTRADE_SECRET_TOKEN = 'MK[gb-NI)]W-7FApv-udj*B-[d.pF-Nf!B.-a4w%j'

HAOBTC_API_KEY = 'bcfada37-f4d7-4810-8720-a7a296f05c85'
HAOBTC_SECRET_TOKEN = '34965149-25f6-4aed-8449-2629168b7895'

BTCC_API_KEY = 'e029c073-8e68-4598-8c63-0f3890579c3e'
BTCC_SECRET_TOKEN = 'bf5b3d87-9f73-4773-9a39-459f1e6bbc74'

BITBAYS_API_KEY = '6C-02BE2FEB-08208FA5-306C69F2'
BITBAYS_SECRET_TOKEN = '3EAFF944767A318504633754BAA8BBA5A91B34C2'

# 检测频率
TickInterval = 0.8  # 监测频率（秒），0.4到此值之间的随机值，应大于0.4

# 主动策略
MinDiff = 1  # 最低差价(元) 因为现在主要利润来自于被动套利策略，此值设高一点，只抓住大机会，为被动套利让时间
SlidePrice = 3  # 止损滑动价(元)，一轮操作完毕后，当总币数出现波动时，需要恢复原币数，买（卖）时加（减）这个值
MinAmountOnce = 0.01  # 主动策略一次交易最少币数
MaxAmountOnce = 0.08  # 主动策略一次交易最多币数
OpenPriorityStrategy = False  # 是否开启主动策略，False表示不开启，True表示开启

# Mysql config
HOST = '127.0.0.1'
PORT = '3306'
USER = 'root'
PASSWORD = '123456'
DATABASE = 'arbitrager'

# Email config
EMAIL_HOST = 'smtp.163.com'
EMAIL_HOST_USER = '15911155376@163.com'
EMAIL_HOST_PASSWORD = '920724sc'  # 这是授权码，不是邮箱账号密码
EMAIL_RECEIVER = ['690023772@qq.com']
TIMES_ONEDAY = 4  # 每天发送邮件的次数，必须能够被24整除：1，2，3，4，6，8，12，24


# 被动策略
# 好比特挂单被成交赚的费率，最新费率千2；吃单扣千4
makerRate = 0.002
takerRate = 0.004
# 对冲的市场（Ok或者火币）费率，千2
tradeRate = 0
# 采用现价只挂单的方式下单，避免吃单损失
# 订单警告系数，考虑到好比特的挂单返利机制，此系数用来缩小返利范围，减少实际操作误差。系数越低，订单有效范围越窄，成交亏损的可能性越小
ORDERWARNINGRATE = 0.6  # 值域[0, 1]，越小失误率越小，但可能错过一些机会。保留一元的滑点缓冲区，设定为0.15
# 正整数，阈值[5,10]，好比特撤单条件之一。订单保留的位置，买一到距离为N元的位置 或者 卖一到距离为N元的位置；
# 例如，当前价bid 5000,ask 5001，此参数为5时，买单和买单有效范围[4996, 5005]
OrderRetainScope = 30
# 订单有效范围，作用为减少撤单次数，值域[2, 5]。例，当前订单在订单保留范围之内，但不是最优价格，但在有效范围之内，不撤销
OrderValidScope = 5

# Bmob，套利数据库
REQURL = "/1/classes/op_record"
HEADERDATA = {"X-Bmob-Application-Id": "1ba3450f350015102707eef2eff716c0",
 "X-Bmob-REST-API-Key": "de3d0c788bc5036c368ee365633caae4",
 "Content-Type": "application/json"}
# Bmob，好比特资产数据库
REQURL1 = "/1/classes/haobtc"
HEADERDATA1 = {"X-Bmob-Application-Id": "306a00325be1f676a6ef50833796072d",
 "X-Bmob-REST-API-Key": "db582175d422385fcfb468aaa510e685",
 "Content-Type": "application/json"}

# 利润记录周期（秒）
PeriodRecordProfit = 65  # N单位分钟 + M半分钟 + 5。建议65秒。这样做的目的是为了尽量避免和fundBalance冲突

# 资金平衡策略
# 每天资金平衡次数
TIMES_BALANCEFUND = 2  # 每天资金平衡的次数，必须能被24整除，1,2,3,4,6,8,12,24
# 资金平衡策略触发价格段。对应的资金差值为totalBTC * ThresholdPrice。
# 此项参数意思是当比特币价值和人民币价值差值超过了ThresholdPrice * totalBTC之后，进行平衡操作。每这么个间断就可以执行。
# 这项值是跟随总币数变动的，当币数少的时候，阈值对应的也越小；反之，越大。
# 阈值范围[50, 200]，值越小，资金平衡策略触发的次数越多，两种资产越平衡，错过交易的最优价格的概率也越大；
# 值越大，资金平衡策略楚大的次数越少，资产可能会出现暂时的不平衡，但有更大概率获取最优价格
ThresholdPrice = 2500
# 触发时间阈值，控制定时任务的触发，比如资金平衡策略、利润统计。过大可能在整点触发多次，过小可能触发不了。值域[2, 4]
TiggerTime = 1

# 用来测试交易所的基本操作是否正常
from markets.okcoin import Okcoin
from markets.huobi import Huobi
from markets.chbtc import Chbtc
from markets.btctrade import Btctrade
from markets.btcc import Btcc
from markets.bitbays import Bitbays
from markets.haobtc import Haobtc


#a = Okcoin()
#a = Huobi()
#a = Chbtc()
#a = Btctrade()
#a = Btcc()
#a = Bitbays()
a = Haobtc()

#print('#ticlker')
#print(a.ticker())


# print('#update depth: %s' % (a.update_depth()))
# print("ask:%f, ask_volume:%f" % (a.ask, a.ask_volume))
# print("bid:%f, bid_volume:%f" % (a.bid, a.bid_volume))
# print()
#
# print('#update account: %s' % (a.update_accountInfo()))
# print("btc_free:%f, btc_frozen:%f, cny_free:%f, cny_frozen:%f" % (a.btc_free, a.btc_frozen, a.cny_free, a.cny_frozen))
# print()

# print("#buy_response:")
# buy_response1 = a.buy(3000, 0.01)
# print(buy_response1)
# buy_response2 = a.buy(3100, 0.01)
# print(buy_response2)
# buy_response3 = a.buy(3200, 0.01)
# print(buy_response3)
# print()
#
# print("#sell_response:")
# sell_response1 = a.sell(6000, 0.01)
# print(sell_response1)
# sell_response2 = a.sell(6100, 0.01)
# print(sell_response2)
# sell_response3 = a.sell(6200, 0.01)
# print(sell_response3)
# print()
#
# print("#order_info")
# print(a.order_info(buy_response1))
# print(a.order_info(sell_response1))
# print()

print("#get_orders")
print(a.get_orders())
print()

# print("#cancel_order")
# print(a.cancel_order(buy_response1))
# print(a.cancel_order(sell_response1))
# print()
#
# print("#cancel_all")
# a.cancel_all()



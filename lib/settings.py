#只需要host，盘口，深度，账户信息，限价买卖，取消订单，获取订单（分为根据ID获取 和 无 ID获取所有当前订单）
#PS：火币的两种获取订单方式分为两种方法，OkCoin的两种获取方式合二为一

#理想期望是下单即成交，下单后，立马查询订单成交情况，如果未成交，撤单，记录下单情况和部分成交信息，然后计算前后两次持仓量之差，平衡之。

#get_orders  获取未成交订单

HUOBI_API_URL = {
    'host': 'https://api.huobi.com/apiv3',
    'depth': 'http://api.huobi.com/staticmarket/depth_btc_1.js',
    'account_info': 'get_account_info',
    'buy': 'buy',
    'sell': 'sell',
    'order_info': 'order_info',
    'get_orders': 'get_orders',
    'cancel_order': 'cancel_order',
    'fee': 0
}

OKCOIN_API_URL = {
    'host': 'www.okcoin.cn', #中国站用cn，国际站用com
    'depth': '/api/v1/depth.do',
    'account_info': '/api/v1/userinfo.do',
    'trade': '/api/v1/trade.do',
    'order_info': '/api/v1/order_info.do',
    'cancel_order': '/api/v1/cancel_order.do',
    'fee': 0
}

CHBTC_API_URL = {
    'host': 'https://trade.chbtc.com/api/',
    'depth': 'http://api.chbtc.com/data/v1/depth?currency=btc_cny',
    'account_info': 'getAccountInfo',
    'trade': 'order',
    'order_info': 'getOrder',
    'get_orders': 'getUnfinishedOrdersIgnoreTradeType',
    'cancelOrder': 'cancelOrder',
    'fee': 0
}

BTCC_API_URL = {
    'host': 'api.btcchina.com',
    'depth': 'https://data.btcchina.com/data/orderbook?limit=1',
    'account_info': 'getAccountInfo',
    'buy': 'buyOrder2',
    'sell': 'sellOrder2',
    'order_info': 'getOrder',
    'get_orders': 'getOrders',
    'cancel_order': 'cancelOrder',
    'fee': 0
}

BTCTRADE_API_URL = {
    'host': 'http://api.btctrade.com/api/',
    'depth': 'depth',
    'account_info': 'balance',
    'buy': 'buy',
    'sell': 'sell',
    'order_info': 'fetch_order',
    'get_orders': 'orders',
    'cancel_order': 'cancel_order',
    'fee': 0
}

BITBAYS_API_URL = {
    'host': 'www.bitbays.com',
    'depth': 'https://bitbays.com/api/v1/depth/?market=btc_cny',
    'account_info': 'https://bitbays.com/api/v1/info/',
    'trade': 'https://bitbays.com/api/v1/trade/',
    'order_info': 'https://bitbays.com/api/v1/order/',
    'get_orders': 'https://bitbays.com/api/v1/orders/',
    'cancel_order': 'https://bitbays.com/api/v1/cancel/',
    'fee': 0
}

HAOBTC_API_URL = {
    'host': 'https://haobtc.com/exchange/api/v1/',
    'depth': 'depth',
    'account_info': 'account_info',
    'trade': 'trade',
    'order_info': 'order_info',
    'get_orders': 'orders_info',
    'cancel_order': 'cancel_order',
    'fee': 0.04 #0.04%
}

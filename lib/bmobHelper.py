import urllib.request
import http.client
import json
import config
import ssl


class BmobHelper(object):
    def __init__(self):
        self.requrl = config.REQURL  # 替换为自己的表名
        self.headerdata = config.HEADERDATA

    def record_op(self, op_time, op_type, buy_platform, sell_platform, price, amount, message):
        if price is None:
            price = 0
        if amount is None:
            amount = 0
        context = ssl._create_unverified_context()
        conn = http.client.HTTPSConnection("api.bmob.cn", context=context)
        data = {'op_time': op_time, 'op_type': op_type, 'buy_platform': buy_platform, 'sell_platform': sell_platform, 'price': price, 'amount': amount, 'message': message};
        data = json.dumps(data)
        print("op_record: %s %s" % (op_time, op_type))
        try:
            conn.request(method="POST", url=self.requrl, body=data, headers=self.headerdata)
        except Exception as e:
            print(e)
            print("Add data error!")

        conn.close()

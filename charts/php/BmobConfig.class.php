<?php
/**
 * bmobConfig 配置文件
 * @author newjueqi
 * @license http://www.gnu.org/copyleft/lesser.html Distributed under the Lesser General Public License (LGPL)
 */
class BmobConfig
{
     const APPID = '1ba3450f350015102707eef2eff716c0';  //后台"应用密钥"中的Application ID
     const RESTKEY = 'de3d0c788bc5036c368ee365633caae4';  //后台"应用密钥"中的REST API Key
     const BMOBURL = 'https://api.bmob.cn/1/';  //请勿修改
     const BMOBURLTWO = 'https://api.bmob.cn/2/';  //请勿修改
}
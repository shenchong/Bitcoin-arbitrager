<?php
//从Bmob中取数据
include_once 'BmobObject.class.php';

$op_record = new BmobObject('op_record');

//获取profit
$res = $op_record->get("", array('where={"op_type":"profit"}','limit=0','count=1'));
$count = $res->count; //统计出profit的总条数
//分页，每页1000条，如有多余页，则将其单做一页
$pages = intval(floor($count/1000));
if ($count%1000 != 0) {
	$pages = $pages + 1;
}
$results = [];
for ($i=0; $i < $pages; $i++) {
	$skip = $i * 1000; 
	$res = $op_record->get("", array('where={"op_type":"profit"}', '$res=$bmobObj->get("",array("keys=op_time,amount,price,message"))', 'order=op_time', 'limit=1000', "skip=$skip"));
	$array = json_decode(json_encode($res->results),TRUE);
	$results = array_merge($results, $array);
}
$datetime_profit = array();
$profit = array();
$price = array();
$fund = array();//保存当前资金总额，比特币数量*价格+人民币数量
$current_BTC = 0;
foreach ($results as $item) {
	$datetime_profit[] = $item['op_time'];
	$profit[] = floatval($item['amount']);
	$price[] = intval($item['price']);
	$temp_price = floatval(substr($item['message'], 54, 7));
	if($temp_price == 0){
		$temp_price = 5550;
	}
	$fund[] = floatval(substr($item['message'], 11, 10)) + floatval(substr($item['message'], 36, 8))*$temp_price;
	$current_BTC = floatval(substr($item['message'], 36, 5));
}
file_put_contents('../data/profit.json', json_encode(array($datetime_profit, $profit, $price, $fund)));


//获取error的个数
$res = $op_record->get("", array('where={"op_type":"error"}','limit=0','count=1'));
$count_error = $res->count; //统计出profit的总条数

//获取volume
$res = $op_record->get("", array('where={"op_type":{"$in":["trade","enbalanceBTC","error"]}}','limit=0','count=1'));
$count = $res->count; //统计出profit的总条数
//分页，每页1000条，如有多余页，则将其单做一页
$pages = intval(floor($count/1000));
if ($count%1000 != 0) {
	$pages = $pages + 1;
}
$results = [];
for ($i=0; $i < $pages; $i++) {
	$skip = $i * 1000;
	$res = $op_record->get("", array('where={"op_type":{"$in":["trade","enbalanceBTC","error"]}}', '$res=$bmobObj->get("",array("keys=op_time,amount,op_type"))', 'order=op_time', 'limit=1000', "skip=$skip"));
	$array = json_decode(json_encode($res->results),TRUE);
	$results = array_merge($results, $array);
}
$datetime_volume = array();
$volume = array();
$op_type = array();
foreach ($results as $item) {
	$datetime_volume[] = $item['op_time'];
	$volume[] = floatval($item['amount']);
	$op_type[] = $item['op_type'];
}
file_put_contents('../data/volume.json', json_encode(array($datetime_volume, $volume, $op_type, $count_error, $current_BTC)));//将op_type类型为error的条数记录在volume.json中的第四条

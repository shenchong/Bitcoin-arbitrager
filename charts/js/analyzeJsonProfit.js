// 基于准备好的dom，初始化echarts实例
var myChart = echarts.init(document.getElementById('totalProfit'));//总利润
var myChart4 = echarts.init(document.getElementById('profitOrigin'));//利润来源比例
var myChart6 = echarts.init(document.getElementById('dailyProfit'));//每日利润统计以及每日折合年化估算
var myChart7 = echarts.init(document.getElementById('fund'));//总资产走势图（估算）
var myChart8 = echarts.init(document.getElementById('monthProfit'));//统计每个月的利润总和

// 指定图表的配置项和数据
var option = {
    title: {
        text: '利润统计'
    },
    tooltip: {
        trigger: 'axis',
        formatter: '{b0}<br />{a0}: {c0}元<br />{a1}: {c1}元'
    },
    legend: {
        data: ['单次利润', '总利润']
    },
    xAxis: {
        data: []
    },
    yAxis: [
        {
            name: '单次利润(元)',
            type: 'value'
        },
        {
            name: '总利润(元)',
            type: 'value'
        }

    ],
    series: [
        {
            name: '单次利润',
            type: 'bar',
            yAxisIndex: 0,
            data: []
        },
        {
            name: '总利润',
            type: 'line',
            yAxisIndex: 1,
            showSymbol: false,
            data: []
        }]
};

var option4 = {
    title: {
        text: '利润来源比例',
        x: 'center'
    },
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: ['被动策略', '主动策略']
    },
    series: [{
        name: '利润来源',
        type: 'pie',
        radius: '55%',
        center: ['50%', '60%'],
        data: [
            { 'value': 335, 'name': '被动策略' },
            { 'value': 310, 'name': '主动策略' }
        ]
    }]
};


var option6 = {
    title: {
        text: '每日利润统计'
    },
    tooltip: {
        trigger: 'axis',
        formatter: '{b0}<br />{a0}: {c0}%<br />{a2}: {c2}元<br />{a1}: {c1}元<br />{a3}: {c3}元'
    },
    legend: {
        data: ['每日被动策略利润', '每日主动策略利润', '折合年化', '总利润']
    },
    xAxis: {
        data: []
    },
    yAxis: [
        {
            name: '折合年化(%)',
            type: 'value'
        },
        {
            name: '每日利润值(元)',
            type: 'value'
        }

    ],
    series: [
        {
            name: '折合年化',
            type: 'line',
            lineStyle: {
                normal: {
                    width: 1,
                    color: '#ff9900'
                }
            },
            showSymbol: false,
            data: []
        },
        {
            name: '每日主动策略利润',
            type: 'bar',
            stack: 1,
            yAxisIndex: 1,
            lineStyle: {
                normal: {
                    width: 1
                }
            },
            showSymbol: false,
            data: []
        },
        {
            name: '每日被动策略利润',
            type: 'bar',
            stack: 1,
            yAxisIndex: 1,
            itemStyle: {
                normal: {
                    width: 1,
                    color: '#c23531'
                }
            },
            showSymbol: false,
            data: []
        },
        {
            name: '总利润',
            type: 'line',
            yAxisIndex: 1,
            lineStyle: {
                normal: {
                    width: 0
                }
            },
            showSymbol: false,
            data: []
        }
    ]
};

var option7 = {
    title: {
        text: '总资产统计'
    },
    tooltip: {
        trigger: 'axis',
        formatter: '{b0}<br />{a0}: {c0}元'
    },
    legend: {
        data: ['总资产走势图（估算）']
    },
    xAxis: {
        data: []
    },
    yAxis: {
        min: 'dataMin'
    },
    series: [{
        name: '总资产走势图（估算）',
        type: 'line',
        lineStyle: {
            normal: {
                width: 1
            }
        },
        showSymbol: false,
        data: []
    }]
};

var option8 = {
    title: {
        text: '月化收益统计'
    },
    tooltip: {
        trigger: 'axis',
        formatter: '{b0}<br />{a0}: {c0}%<br />{a1}: {c1}元'
    },
    legend: {
        data: ['每月利润值', '月化收益率']
    },
    xAxis: {
        data: []
    },
    yAxis: [
        {
            name: '月化收益率(%)',
            type: 'value'
        },
        {
            name: '每月利润值(元)',
            type: 'value'
        }

    ],
    series: [
        {
            name: '月化收益率',
            type: 'line',
            lineStyle: {
                normal: {
                    width: 1
                }
            },
            showSymbol: false,
            data: []
        },
        {
            name: '每月利润值',
            type: 'bar',
            yAxisIndex: 1,
            lineStyle: {
                normal: {
                    width: 1
                }
            },
            showSymbol: false,
            data: []
        }]
};

$(document).ready(function () {
    $.get('data/profit.json', { stamp: Math.random() }, function (data) {

        var temp_profitAmount = [];
        var profit_ratio = [{ "value": 0, "name": '被动策略' }, { "value": 0, "name": '主动策略' }];//利润来源比例
        var profit_Haobtc = 0;
        var profit_Others = 0;
        //统计每日利润以及折合年化
        var i = 0;//计日器
        var sum_haobtc = 0;//单日好比特利润
        var sum_others = 0;//单日其他交易所利润
        var sum_profit = 0;//记录单日总利润
        var fund = [];//估计的资金总量
        var date = [];//记录当前日期，精确到日
        var daily_haobtc_profit = [];//每日来自于好比特的利润
        var daily_others_profit = [];//每日来自于其他市场的利润
        var daily_profit = [];//记录每天的总利润
        var ratio = [];//折合年化
        //统计每月收益以及折合年化
        var j = 0;//计月器
        var sum_monthly = 0;//单月利润总和
        var month = [];//记录当前月份
        var month_profit = [];
        var monthly_ratio = [];//当月收益占总资产的百分比
        date[i] = data[0][0].substr(0, 10);
        fund[i] = data[3][0];
        month[j] = data[0][0].substr(0, 7);
        for (var index = 0; index < data[1].length; index++) {
            if (index == 0) {
                temp_profitAmount[0] = data[1][0];
            }
            else {
                temp_profitAmount[index] = parseFloat((temp_profitAmount[index - 1] + data[1][index]).toFixed(6));
            }
            if (data[2][index] == 0) {
                profit_Haobtc += data[1][index];//标志为0，利润来自Haobtc
            } else {
                profit_Others += data[1][index];//标志为1，利润来自Others
            }
            //统计每日利润以及折合年化
            if (date[i] == data[0][index].substr(0, 10)) {
                if (data[2][index] == 0) {
                    sum_haobtc += data[1][index];//标志为0，利润来自Haobtc
                } else {
                    sum_others += data[1][index];//标志为1，利润来自Others
                }
                sum_profit += data[1][index];
            }
            else {
                daily_haobtc_profit[i] = parseFloat(sum_haobtc.toFixed(2));//标志为0，利润来自Haobtc
                daily_others_profit[i] = parseFloat(sum_others.toFixed(2));//标志为1，利润来自Others
                daily_profit[i] = parseFloat(sum_profit.toFixed(2));
                ratio[i] = parseFloat(((daily_haobtc_profit[i]+daily_others_profit[i]) / fund[i] * 365 * 100).toFixed(2));
                if (data[2][index] == 0) {
                    sum_haobtc = data[1][index];//标志为0，利润来自Haobtc
                    sum_others = 0;//标志为1，利润来自Others
                } else {
                    sum_haobtc = 0;//标志为0，利润来自Haobtc
                    sum_others = data[1][index];//标志为1，利润来自Others
                }
                sum_profit = data[1][index];
                i++;
                date[i] = data[0][index].substr(0, 10);
                fund[i] = data[3][index];
            }
            //统计每月收益以及折合年化
            if (month[j] == data[0][index].substr(0, 7)) {
                sum_monthly += data[1][index];
            } else {
                month_profit[j] = parseFloat(sum_monthly.toFixed(2));
                monthly_ratio[j] = parseFloat((month_profit[j] / fund[i] * 100).toFixed(2));
                sum_monthly = data[1][index];
                j++;
                month[j] = data[0][index].substr(0, 7);
            }
        }
        //添上最后一天的
        daily_haobtc_profit[i] = parseFloat(sum_haobtc.toFixed(2));
        daily_others_profit[i] = parseFloat(sum_others.toFixed(2));
        daily_profit[i] = parseFloat(sum_profit.toFixed(2));
        ratio[i] = parseFloat(((daily_haobtc_profit[i]+daily_others_profit[i]) / fund[i] * 365 * 100).toFixed(2));
        //添上最后一月的
        month_profit[j] = parseFloat(sum_monthly.toFixed(2));
        monthly_ratio[j] = parseFloat((month_profit[j] / fund[i] * 100).toFixed(2));

        profit_ratio[0]['value'] = parseFloat(profit_Haobtc.toFixed(4));
        profit_ratio[1]['value'] = parseFloat(profit_Others.toFixed(4));
        option4.series[0].data = profit_ratio;

        option.xAxis.data = data[0].slice(-5000);//只取最近5000条数据
        option.series[0].data = data[1].slice(-5000);
        option.series[1].data = temp_profitAmount.slice(-5000);

        option6.xAxis.data = date.slice(-10);
        option6.series[0].data = ratio.slice(-10);
        //option6.series[1].data = daily_profit.slice(-10);
        option6.series[1].data = daily_others_profit.slice(-10);//主动策略
        option6.series[2].data = daily_haobtc_profit.slice(-10);//被动策略
        option6.series[3].data = daily_profit.slice(-10);//每日总利润

        option7.xAxis.data = date.slice(-10);
        option7.series[0].data = fund.slice(-10);

        option8.xAxis.data = month;
        option8.series[0].data = monthly_ratio;
        option8.series[1].data = month_profit;

        myChart.setOption(option);
        myChart4.setOption(option4);
        myChart6.setOption(option6);
        myChart7.setOption(option7);
        myChart8.setOption(option8);
    });

});
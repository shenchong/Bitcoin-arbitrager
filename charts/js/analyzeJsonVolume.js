// 基于准备好的dom，初始化echarts实例
var myChart1 = echarts.init(document.getElementById('totalVolume'));//总交易量
var myChart5 = echarts.init(document.getElementById('volumeOrigin'));//交易量来源比例
var myChart9 = echarts.init(document.getElementById('tradeAnalyze'));//分析交易量

var option1 = {
    title: {
        text: '交易量统计'
    },
    tooltip: {
        trigger: 'axis',
        formatter: '{b0}<br />{a0}: {c0} BTC<br />{a1}: {c1} BTC'
    },
    legend: {
        data: ['单次交易量', '总交易量']
    },
    xAxis: {
        data: []
    },
    yAxis: [
        {
            name: '单次交易量(BTC)',
            type: 'value'
        },
        {
            name: '总交易量(BTC)',
            type: 'value'
        }

    ],
    series: [
        {
            name: '单次交易量',
            type: 'bar',
            yAxisIndex: 0,
            data: []
        },
        {
            name: '总交易量',
            type: 'line',
            yAxisIndex: 1,
            showSymbol: false,
            data: []
        }]
};


var option5 = {
    title: {
        text: '交易量来源比例',
        x: 'center'
    },
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: ['被动策略', '主动策略']
    },
    series: [{
        name: '交易量来源',
        type: 'pie',
        radius: '55%',
        center: ['50%', '60%'],
        data: [
            { value: 335, name: '被动策略' },
            { value: 310, name: '主动策略' }
        ]
    }]
};

var option9 = {
    title: {
        text: '资金利用率分析'
    },
    tooltip: {
        trigger: 'axis'
        //formatter: '{b0}<br />{a0}: {c0} BTC<br />{a1}: {c1} BTC<br />{a2}: {c2}%<br />{a3}: {c3}%<br />{a4}: {c4}%'
    },
    legend: {
        data: ['主动策略交易量', '被动策略交易量', '主动策略资金利用率', '被动策略资金利用率', '主动策略失败率']
    },
    xAxis: {
        data: []
    },
    yAxis: [
        {
            name: '交易量(BTC)',
            type: 'value'
        },
        {
            name: '利用率(%)',
            type: 'value'
        }

    ],
    series: [
        {
            name: '主动策略交易量',
            type: 'bar',
            stack: 1,
            yAxisIndex: 0,
            itemStyle: {
                normal: {
                    width: 1,
                    color: '#2f4554'
                }
            },
            data: []
        },
        {
            name: '被动策略交易量',
            type: 'bar',
            stack: 1,
            yAxisIndex: 0,
            itemStyle: {
                normal: {
                    width: 1,
                    color: '#c23531'
                }
            },
            showSymbol: false,
            data: []
        },
        {
            name: '主动策略资金利用率',
            type: 'line',
            yAxisIndex: 1,
            lineStyle: {
                normal: {
                    color: '#993366'
                }
            },
            data: []
        },
        {
            name: '被动策略资金利用率',
            type: 'line',
            yAxisIndex: 1,
            lineStyle: {
                normal: {
                    color: '#a4c2f4'
                }
            },
            data: []
        },
        {
            name: '主动策略失败率',
            type: 'line',
            yAxisIndex: 1,
            data: []
        }
    ]
};


$(document).ready(function () {
    $.get('data/volume.json', { stamp: Math.random() }, function (data) {

        var temp_volumeAmount = [];
        var volume_ratio = [{ "value": 0, "name": '被动策略' }, { "value": 0, "name": '主动策略' }];
        var volume_trade = 0;
        var volume_enbalanceBTC = 0;
        var counter_enbalanceBTC = 0;//统计enbalanceBTC的总次数
        //统计每天的两种策略的交易量
        var i = 0;//计日器
        var date = [];//每日日期
        var current_BTC = data[4];//系统中当前比特币的持有量
        var daily_volume_enbalanceBTC = [];//每日enbalanceBTC操作的比特币数量
        var daily_volume_trade = [];//每日trade操作的比特币数量
        var daily_counter_error = [];//每日error的次数
        var daily_counter_enbalanceBTC = [];//每日enbalanceBTC的次数
        var daily_counter_trade = [];//每日trade的次数
        var counter_ER = 0;//临时保存每日error的次数
        var counter_E = 0;//临时保存每日enbalanceBTC的次数
        var counter_T = 0;//临时保存每日trade的次数
        var sum_daily_enbalanceBTC = 0;//临时保存每日enbalanceBTC操作的数量
        var sum_daily_trade = 0;//临时保存每日trade操作的数量
        var daily_Haobtc_fund_use_rate = [];//每日被动策略资金利用率
        var daily_Others_fund_use_rate = [];//每日主动策略资金利用率
        var daily_Others_failure_rate = [];//每日主动策略失败率

        date[i] = data[0][0].substr(0, 10);

        for (var index = 0; index < data[1].length; index++) {
            if (index == 0) {
                temp_volumeAmount[0] = data[1][0];
            }
            else {
                temp_volumeAmount[index] = parseFloat((temp_volumeAmount[index - 1] + data[1][index]).toFixed(4));
            }
            if (data[2][index] == 'enbalanceBTC') {
                volume_enbalanceBTC += data[1][index];
                counter_enbalanceBTC++;
            } else {
                volume_trade += data[1][index];
            }

            //统计每日的交易量
            if(date[i] == data[0][index].substr(0, 10)){
                if(data[2][index] == "enbalanceBTC"){
                    counter_E ++;
                    sum_daily_enbalanceBTC += data[1][index];
                }
                if(data[2][index] == "trade"){
                    counter_T ++;
                    sum_daily_trade += data[1][index];
                }
                if(data[2][index] == "error"){
                    counter_ER ++;
                }
            }else{
                daily_volume_enbalanceBTC[i] = parseFloat((sum_daily_enbalanceBTC).toFixed(2));
                daily_volume_trade[i] = parseFloat((sum_daily_trade).toFixed(2));
                daily_counter_error[i] = counter_ER;
                daily_counter_enbalanceBTC[i] = counter_E;
                daily_counter_trade[i] = counter_T;
                
                if(data[2][index] == "enbalanceBTC"){
                    counter_E = 1;
                    sum_daily_enbalanceBTC = data[1][index];
                }else{
                    counter_E = 0;
                    sum_daily_enbalanceBTC = 0;
                }

                if(data[2][index] == "trade"){
                    counter_T = 1;
                    sum_daily_trade = data[1][index];
                }else{
                    counter_T = 0;
                    sum_daily_trade = 0;
                }
                
                if(data[2][index] == "error"){
                    counter_ER = 1;
                }else{
                    counter_ER = 0;
                }
                
                i++;
                date[i] = data[0][index].substr(0, 10);
            }
        }
        //补上最后一天的
        daily_volume_enbalanceBTC[i] = parseFloat((sum_daily_enbalanceBTC).toFixed(2));
        daily_volume_trade[i] = parseFloat((sum_daily_trade).toFixed(2));
        daily_counter_error[i] = counter_ER;
        daily_counter_enbalanceBTC[i] = counter_E;
        daily_counter_trade[i] = counter_T;

        //分析交易量
        for(var j=0;j<date.length;j++){
            //主动策略的资金量为总资金量的3%（根据实际资金量变化而变化）
            daily_Others_fund_use_rate[j] = parseFloat((daily_volume_trade[j]/(current_BTC*0.03)/2*100).toFixed(2));
            //被动策略的资金量为总资金量的97%（根据实际资金量变化而变化）
            daily_Haobtc_fund_use_rate[j] = parseFloat((daily_volume_enbalanceBTC[j] * (1-daily_counter_error[j]/daily_counter_enbalanceBTC[j]) / (current_BTC*0.97) / 2 * 100).toFixed(2));
            daily_Others_failure_rate[j] = parseFloat((daily_counter_error[j]/daily_counter_trade[j]*100).toFixed(2));
        }
        option9.xAxis.data = date.slice(-10);
        option9.series[0].data = daily_volume_trade.slice(-10);
        option9.series[1].data = daily_volume_enbalanceBTC.slice(-10);
        option9.series[2].data = daily_Others_fund_use_rate.slice(-10);
        option9.series[3].data = daily_Haobtc_fund_use_rate.slice(-10);
        option9.series[4].data = daily_Others_failure_rate.slice(-10);

        //主动策略的交易量
        volume_trade = parseFloat(((1 + (data[3] / counter_enbalanceBTC)) * volume_trade).toFixed(4));
        //被动策略的交易量为enbalanceBTC的量减去因为主动策略出错而平衡的量
        volume_enbalanceBTC = parseFloat((volume_enbalanceBTC - (data[3] / counter_enbalanceBTC) * volume_trade).toFixed(4));
        volume_ratio[0]['value'] = volume_enbalanceBTC;
        volume_ratio[1]['value'] = volume_trade;
        option5.series[0].data = volume_ratio;

        option1.xAxis.data = data[0].slice(-3000);//只取最近3000条数据
        option1.series[0].data = data[1].slice(-3000);
        option1.series[1].data = temp_volumeAmount.slice(-3000);

        myChart1.setOption(option1);
        myChart5.setOption(option5);
        myChart9.setOption(option9);
    });
});
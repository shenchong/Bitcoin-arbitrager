**数据库名为arbitrager，只需要一张表op_record即可，下面是这张表的创建方法**

```
drop table if exists op_record;

/*==============================================================*/
/* Table: op_record                                             */
/*==============================================================*/
create table op_record
(
   op_time              datetime,
   op_type              varchar(20),
   buy_platform         varchar(10),
   sell_platform        varchar(10),
   price                float,
   amount               float,
   message              varchar(50)
);

alter table op_record comment '操作记录，包括正常操作与错误处理
op_type: trade, error, balance, profi';
```
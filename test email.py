# 用来测试邮件功能是否正常
from lib.mysqlhelper import MysqlHelper
import config
import time
import smtplib

query = MysqlHelper()
subject = 'Arbitrage statistics'
datetime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))
msg = "%s: profit in 24h is %.5f" % (datetime, query.getProfitIn24H())
query.end()
message = "From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n%s\r\n" % \
          (config.EMAIL_HOST_USER, ", ".join(config.EMAIL_RECEIVER), subject, msg)
try:
    smtpserver = smtplib.SMTP(config.EMAIL_HOST)
    smtpserver.set_debuglevel(0)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.login(config.EMAIL_HOST_USER, config.EMAIL_HOST_PASSWORD)
    smtpserver.sendmail(config.EMAIL_HOST_USER, config.EMAIL_RECEIVER, message)
    smtpserver.quit()
    smtpserver.close()
    print("%s: Send arbitrage statistics email successfully!" % datetime)
except Exception as e:
    print(e)
    print("%s: Send arbitrage statistics email failed!" % datetime)
time.sleep(10 - config.TickInterval)
